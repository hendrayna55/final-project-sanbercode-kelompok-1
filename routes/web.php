<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TopUpDiamondIDServerController;
use App\Http\Controllers\KalkulatorMLController;
use App\Http\Controllers\Login\AuthController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AkunController;
use App\Http\Controllers\NominalController;
use App\Http\Controllers\MetodePembayaranController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Dashboard
Route::get('/login', [AuthController::class, 'index'])->name('login');
Route::post('/postlogin', [AuthController::class, 'login']);

Route::middleware(['auth', 'verified_user', 'admin'])->group(function () {
    Route::controller(AdminController::class)->group(function (){
        Route::get('/dashboard', 'dashboard');
        // Route::get('/user', 'user');
        // Route::get('/nominal', 'nominal');
        // Route::get('/metode_pembayaran', 'pembayaran');
        // Route::get('/order', 'order');
        // Route::get('/user', 'index_user');

        Route::prefix('/user')->group(function() 
        {
            Route::get('/',[AdminController::class,'user']);
            Route::get('/create',[AdminController::class,'createUser']);
            Route::post('/',[AdminController::class,'storeUser']);
            Route::get('/{user_id}/edit',[AdminController::class,'editUser']);
            Route::put('/{user_id}',[AdminController::class,'updateUser']);
            Route::delete('/{user_id}',[AdminController::class,'destroyUser']);
        });

        Route::prefix('/data-akun')->group(function() 
        {
            Route::get('/',[AdminController::class,'akun']);
            Route::get('/create',[AdminController::class,'createAkun']);
            Route::post('/',[AdminController::class,'storeAkun']);
            Route::get('/{akun_id}/edit',[AdminController::class,'editAkun']);
            Route::put('/{akun_id}',[AdminController::class,'updateAkun']);
            Route::delete('/{akun_id}',[AdminController::class,'destroyAkun']);
        });

        Route::prefix('/nominal')->group(function() 
        {
            Route::get('/',[AdminController::class,'nominal']);
            Route::get('/create',[AdminController::class,'createProduk']);
            Route::post('/',[AdminController::class,'storeProduk']);
            Route::get('/{nominal_id}/edit',[AdminController::class,'editProduk']);
            Route::put('/{nominal_id}',[AdminController::class,'updateProduk']);
            Route::delete('/{nominal_id}',[AdminController::class,'destroyProduk']);
        });

        Route::prefix('/metode-pembayaran')->group(function() 
        {
            Route::get('/',[AdminController::class,'indexMetodePembayaran']);
            Route::get('/create',[AdminController::class,'createMetodePembayaran']);
            Route::post('/',[AdminController::class,'storeMetodePembayaran']);
            Route::get('/{metodePembayaran_id}/edit',[AdminController::class,'editMetodePembayaran']);
            Route::put('/{metodePembayaran_id}',[AdminController::class,'updateMetodePembayaran']);
            Route::delete('/{metodePembayaran_id}',[AdminController::class,'destroyMetodePembayaran']);
        });

        Route::prefix('/order')->group(function() 
        {
            Route::get('/',[AdminController::class,'indexOrder']);
            Route::get('/create',[AdminController::class,'createOrder']);
            Route::post('/',[AdminController::class,'storeOrder']);
            Route::get('/{order_id}/edit',[AdminController::class,'editOrder']);
            Route::put('/{order_id}',[AdminController::class,'updateOrder']);
            Route::delete('/{order_id}',[AdminController::class,'destroyOrder']);
        });
    });
});
 
Route::middleware(['auth', 'verified_user'])->group(function () {
    Route::get('/top-up-id',[TopUpDiamondIDServerController::class,'index']);
    Route::get('/', [TopUpDiamondIDServerController::class, 'index']);
    Route::post('/order-diamond-idserver',[TopUpDiamondIDServerController::class,'store']);

    Route::get('/data-order',[HomeController::class,'dataOrder']);
    Route::get('/contactus',[HomeController::class,'contactUs']);
    
    Route::prefix('kalkulatorml')->group(function() 
    {
        Route::get('/win-rate',[KalkulatorMLController::class,'winRate']);
        Route::get('/magic-wheel',[KalkulatorMLController::class,'magicWheel']);
        Route::get('/zodiac',[KalkulatorMLController::class,'zodiac']);
    });

    Route::prefix('profile')->group(function() 
    {
        Route::get('/{user_id}',[ProfileController::class,'index']);
        Route::get('/{user_id}/edit',[ProfileController::class,'editProfile']);
        Route::put('/{user_id}',[ProfileController::class,'updateProfile']);
    });

    Route::prefix('akun')->group(function() 
    {
        Route::get('/{user_id}/create',[ProfileController::class,'createAkun']);
        Route::post('/',[ProfileController::class,'storeAkun']);
        Route::get('/{akun_id}/edit',[ProfileController::class,'editAkun']);
        Route::put('/{akun_id}',[ProfileController::class,'updateAkun']);
        Route::delete('/{akun_id}',[ProfileController::class,'destroyAkun']);
    });


});

Auth::routes();