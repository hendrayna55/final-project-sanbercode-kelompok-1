# Final Project
## Kelompok 1
## Anggota Kelompok
- Hendra Yanuari Noer Ahmadillah
- Dani Juhaeni
- Muhammad Fathiir Farhansyah

## Tema Project

Website Top Up Game Online

## ERD

![Alt text](https://i.ibb.co/ngtXHhk/ERD-Final-Project.png)

## Link Video

[Link Demo Aplikasi](https://youtu.be/Maih-VGOoAw)

[Link Deploy](http://kenjistore.site/public/)