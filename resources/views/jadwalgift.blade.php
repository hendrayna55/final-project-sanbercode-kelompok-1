<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.head')
    <title>Jadwal Gift</title>
    <!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/dataTables.bootstrap4.min.css"> -->
</head>
<body style="background-color:#343a40">
    @include('layouts.navbar')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <img class="mx-auto d-block" style="width:15%" src="{{asset('assets/img/Kenji-Store.png')}}" alt="">
            </div>
            <div class="pt-4 col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-sm-12">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">
                                Jadwal Gift Skin
                            </h5>
                            <div class="card-body">
                                <table id="example" class="table table-striped table-bordered" style=" overflow: scroll;">
                                    <thead class="text-dark">
                                        <tr class="bg-light" style="background-color:#282c30; border-color:#343a40;">
                                            <th style="border-color:#212529;" class="align-middle text-center">Order Date</th>
                                            <th style="border-color:#212529;" class="align-middle text-center">Nickname</th>
                                            <th style="border-color:#212529;" class="align-middle text-center">User ID</th>
                                            <th style="border-color:#212529;" class="align-middle text-center">Gift Date</th>
                                            <th style="border-color:#212529;" class="align-middle text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-white text-sm">
                                        <tr class="">
                                            <td style="border-color:#343a40;" class="align-middle">Tiger Nixon</td>
                                            <td style="border-color:#343a40;" class="align-middle">System Architect</td>
                                            <td style="border-color:#343a40;" class="align-middle">Edinburgh</td>
                                            <td style="border-color:#343a40;" class="align-middle">2011-04-25</td>
                                            <td style="border-color:#343a40;" class="align-middle">
                                                <div class="btn btn-sm btn-success rounded text-sm" aria-disabled="true">Done</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;" class="align-middle">Tiger Nixon</td>
                                            <td style="border-color:#343a40;" class="align-middle">System Architect</td>
                                            <td style="border-color:#343a40;" class="align-middle">Edinburgh</td>
                                            <td style="border-color:#343a40;" class="align-middle">2011-04-25</td>
                                            <td style="border-color:#343a40;" class="align-middle">
                                                <div class="btn btn-sm btn-warning rounded">Proccess</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Kontol Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        </tr>
                                        <tr>
                                            <td style="border-color:#343a40;">Tiger Nixon</td>
                                            <td style="border-color:#343a40;">System Architect</td>
                                            <td style="border-color:#343a40;">Edinburgh</td>
                                            <td style="border-color:#343a40;">61</td>
                                            <td style="border-color:#343a40;">2011-04-25</td>
                                        </tr>
                                        
                                    </tbody>
                                    <!-- <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                    </tfoot> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')
    @include('layouts.js')
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
</body>
</html>