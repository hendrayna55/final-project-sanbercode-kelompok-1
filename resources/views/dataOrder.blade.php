@extends('layouts.master')

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@push('script')
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.13.4/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <img class="mx-auto d-block" style="width:15%" src="{{asset('assets/img/Kenji-Store.png')}}" alt="">
            </div>
            <div class="pt-4 col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-sm-12">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">
                                Data Order Diamond
                            </h5>
                            <div class="card-body">
                                <table id="example" class="table table-striped table-bordered" style=" overflow: scroll;">
                                    <thead class="text-dark">
                                        <tr class="bg-light" style="background-color:#282c30; border-color:#343a40;">
                                            <th style="border-color:#212529;" class="align-middle text-center">Order Date</th>
                                            <th style="border-color:#212529;" class="align-middle text-center">Denom Diamond</th>
                                            <th style="border-color:#212529;" class="align-middle text-center">Nickname</th>
                                            <th style="border-color:#212529;" class="align-middle text-center">User ID - Server</th>
                                            <th style="border-color:#212529;" class="align-middle text-center">No. Invoice</th>
                                            <th style="border-color:#212529;" class="align-middle text-center">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-white text-sm">
                                        @foreach ($orders as $order)
                                            <tr class="">
                                                <td style="border-color:#343a40;" class="align-middle">{{$order->created_at}}</td>
                                                <td style="border-color:#343a40;" class="align-middle text-center">{{$order->produk->nama}} <i class="fas fa-gem"></i></td>
                                                <td style="border-color:#343a40;" class="align-middle text-center">{{$order->nickname}}</td>
                                                <td style="border-color:#343a40;" class="align-middle text-center">{{$order->id_akun}} ({{$order->server_akun}})</td>
                                                <td style="border-color:#343a40;" class="align-middle text-center">{{ substr($order->bukti_pembayaran, 0, 24)}}</td>
                                                <td style="border-color:#343a40;" class="align-middle text-center">
                                                    @if ($order->status == 'Done')
                                                        <div class="btn btn-sm btn-success rounded text-sm">Done</div>
                                                    @elseif($order->status == 'Process')
                                                        <div class="btn btn-sm btn-warning rounded text-sm">Process</div>
                                                    @else
                                                        <div class="btn btn-sm btn-danger rounded text-sm">Cancel</div>
                                                    @endif
                                                </td>
                                            </tr>
                                            
                                        @endforeach
                                    </tbody>
                                    <!-- <tfoot>
                                        <tr>
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Office</th>
                                            <th>Age</th>
                                            <th>Start date</th>
                                            <th>Salary</th>
                                        </tr>
                                    </tfoot> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection