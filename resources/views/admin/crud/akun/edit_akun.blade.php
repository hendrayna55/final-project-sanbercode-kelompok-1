@extends('admin.master_admin')

@section('title')
    Edit Akun
@endsection

@section('content')
    <form action="{{url('/data-akun/' . $akun->id)}}" method="post" class="m-3">
        @csrf
        @method('put')

        <div class="form-group">
            <label>User ID</label>
            <select name="user_id" id="" class="form-control @error('user_id') is-invalid @enderror">
                <option value="">-- pilih user --</option>
                @foreach ($users as $item)
                    <option value="{{$item->id}}"<?php if($akun->user_id == $item->id) echo 'selected' ?>>{{$item->username}} - {{$item->profile->nama}}</option>
                @endforeach
            </select>
        </div>
        @error('user_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Nickname</label>
            <input type="text" class="form-control @error('nickname') is-invalid @enderror" name="nickname"
                value="{{ $akun->nickname }}">
        </div>
        @error('nickname')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Id Akun</label>
            <input type="number" class="form-control @error('id_akun') is-invalid @enderror" name="id_akun"
                value="{{ $akun->id_akun }}">
        </div>
        @error('id_akun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Server Akun</label>
            <input type="number" class="form-control @error('server_akun') is-invalid @enderror" name="server_akun"
                value="{{ $akun->server_akun }}">
        </div>
        @error('server_akun')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
