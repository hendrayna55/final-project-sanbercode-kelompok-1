@extends('admin.master_admin')

@section('title')
    Detail Akun
@endsection

@section('content')
    <table class="table m-3">
        <thead>
            <tr>
                <th scope="col">Nickname</th>
                <th scope="col">ID Akun</th>
                <th scope="col">Server Akun</th>
                <th scope="col">User ID</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $akun->nickname }}</td>
                <td>{{ $akun->id_akun }}</td>
                <td>{{ $akun->server_akun }}</td>
                <td>{{ $akun->user_id }}</td>
            </tr>

            @stack('scripts')
        </tbody>
    </table>
@endsection
