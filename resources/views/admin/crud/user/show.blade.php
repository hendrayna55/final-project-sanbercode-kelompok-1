@extends('admin.master_admin')

@section('title')
    Detail User
@endsection

@section('content')
    <table class="table m-3">
        <thead>
            <tr>
                <th scope="col">Username</th>
                <th scope="col">Email</th>
                <th scope="col">Roles</th>

            </tr>
        </thead>
        <tbody>
            <tr>
                <td>{{ $user->username }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->role_id }}</td>

                <td>
                </td>
            </tr>
        </tbody>
    </table>
@endsection
