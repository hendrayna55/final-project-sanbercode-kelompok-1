@extends('admin.master_admin')

@section('title')
    Edit User
@endsection

@section('content')
    <form action="{{url('/user/'. $user->id)}}" method="post" class="m-3">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username"
                value="{{ $user->username }}">
            <input type="text" class="d-none form-control @error('username') is-invalid @enderror" name="verified_status" value="{{ $user->verified_status }}">
        </div>
        @error('username')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                value="{{ $user->email }}">
        </div>
        @error('email')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Role</label>
            <select class="custom-select" name="role_id">
                <option value="1" @selected($user->role_id == '1')>Admin</option>
                <option value="2" @selected($user->role_id == '2')>Reseller</option>
                <option value="3" @selected($user->role_id == '3')>User</option>
            </select>
        </div>
        @error('role_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
{{-- <option value="2" {{($post->size === '2') ? 'Selected' : ''}}>2</option> --}}
