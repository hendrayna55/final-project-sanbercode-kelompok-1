@extends('admin.master_admin')

@section('title')
    Tambah Order
@endsection

@section('content')
    <form action="{{url('/order')}}" method="post" class="m-3" enctype="multipart/form-data">
        @csrf
        
        <div class="form-group">
            <label>User</label>
            <select name="users_id" id="" class="form-control @error('users_id') is-invalid @enderror rounded">
                <option value="">-- pilih user --</option>
                
                @foreach ($users as $item)
                    <option value="{{$item->id}}">{{$item->username}}</option>
                @endforeach
            </select>
        </div>
        @error('users_id')
            <div class="alert alert-danger">Nama Metode Pembayaran wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Nickname</label>
            <input type="text" class="form-control @error('nickname') is-invalid @enderror" name="nickname" value="{{old('nickname')}}" placeholder="Masukkan Nickname Akun">
        </div>
        @error('nickname')
            <div class="alert alert-danger">Nickname wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Denom Diamond</label>
            <select name="diamond_idserver_id" id="" class="form-control @error('diamond_idserver_id') is-invalid @enderror rounded">
                <option value="">-- pilih denom diamond --</option>
                
                @foreach ($produks as $item)
                    <option value="{{$item->id}}">{{$item->nama}} Diamond</option>
                @endforeach
            </select>
        </div>
        @error('diamond_idserver_id')
            <div class="alert alert-danger">Pilih Denom Diamond</div>
        @enderror

        <div class="form-group">
            <label>Status</label>
            <select name="status" id="" class="form-control @error('status') is-invalid @enderror rounded">
                <option value="">-- pilih status --</option>

                <option value="Done">Done</option>
                <option value="Cancel">Cancel</option>
                <option value="Process">Process</option>
            </select>
        </div>
        @error('status')
            <div class="alert alert-danger">Pilih Status</div>
        @enderror

        <div class="form-group">
            <label>ID Akun</label>
            <input type="number" class="form-control @error('id_akun') is-invalid @enderror" name="id_akun" value="{{old('id_akun')}}" placeholder="Masukkan ID Akun">
        </div>
        @error('id_akun')
            <div class="alert alert-danger">ID Akun wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Server Akun</label>
            <input type="number" class="form-control @error('server_akun') is-invalid @enderror" name="server_akun" value="{{old('server_akun')}}" placeholder="Masukkan Server Akun">
        </div>
        @error('server_akun')
            <div class="alert alert-danger">Server Akun wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Metode Pembayaran</label>
            <select name="payment_id" id="" class="form-control @error('payment_id') is-invalid @enderror rounded">
                <option value="">-- pilih metode pembayaran --</option>
                
                @foreach ($payments as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        @error('payment_id')
            <div class="alert alert-danger">Pilih Denom Diamond</div>
        @enderror

        <div class="form-group">
            <label>Nomor WhatsApp</label>
            <input type="number" class="form-control @error('nomor_wa') is-invalid @enderror" name="nomor_wa" value="{{old('nomor_wa')}}" placeholder="Masukkan Nomor WhatsApp">
        </div>
        @error('nomor_wa')
            <div class="alert alert-danger">Nomor WhatsApp wajib diisi</div>
        @enderror

        <div class="form-group row">
            <label class="col-12">Bukti Pembayaran</label>
            <input type="file" name="bukti_pembayaran" class="form-control-file @error('bukti_pembayaran') is-invalid @enderror">
        </div>
        @error('bukti_pembayaran')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror

        <button type="submit" class="btn btn-primary mb-4">Submit</button>
    </form>
@endsection
