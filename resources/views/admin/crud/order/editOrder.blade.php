@extends('admin.master_admin')

@section('title')
    Edit Order
@endsection

@section('content')
    <form action="{{url('/order/' . $order->id)}}" method="post" class="m-3" enctype="multipart/form-data">
        @csrf
        @method('put')
        
        <div class="form-group">
            <label>User</label>
            <select name="users_id" id="" class="form-control @error('users_id') is-invalid @enderror rounded">
                <option value="">-- pilih user --</option>
                
                @foreach ($users as $item)
                    <option value="{{$item->id}}" <?php if($order->users_id == $item->id) echo 'selected' ?>>{{$item->username}} - {{$item->profile->nama}}</option>
                @endforeach
            </select>
        </div>
        @error('users_id')
            <div class="alert alert-danger">Nama Metode Pembayaran wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Nickname</label>
            <input type="text" class="form-control @error('nickname') is-invalid @enderror" name="nickname" value="{{old('nickname')?old('nickname'):$order->nickname}}">
        </div>
        @error('nickname')
            <div class="alert alert-danger">Nickname wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Denom Diamond</label>
            <select name="diamond_idserver_id" id="" class="form-control @error('diamond_idserver_id') is-invalid @enderror rounded">
                <option value="">-- pilih user --</option>
                
                @foreach ($produks as $item)
                    <option value="{{$item->id}}" <?php if($order->diamond_idserver_id == $item->id) echo 'selected' ?>>{{$item->nama}} Diamond</option>
                @endforeach
            </select>
        </div>
        @error('diamond_idserver_id')
            <div class="alert alert-danger">Pilih Denom Diamond</div>
        @enderror

        <div class="form-group">
            <label>Status</label>
            <select name="status" id="" class="form-control @error('status') is-invalid @enderror rounded">
                <option value="">-- pilih status --</option>

                <option value="Done" <?php if($order->status == "Done") echo 'selected' ?>>
                    Done
                </option>
                <option value="Cancel" <?php if($order->status == "Cancel") echo 'selected' ?>>
                    Cancel
                </option>
                <option value="Process" <?php if($order->status == "Process") echo 'selected' ?>>
                    Process
                </option>
            </select>
        </div>
        @error('status')
            <div class="alert alert-danger">Pilih Denom Diamond</div>
        @enderror

        <div class="form-group">
            <label>ID Akun</label>
            <input type="number" class="form-control @error('id_akun') is-invalid @enderror" name="id_akun" value="{{old('id_akun')?old('id_akun'):$order->id_akun}}">
        </div>
        @error('id_akun')
            <div class="alert alert-danger">ID Akun wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Server Akun</label>
            <input type="number" class="form-control @error('server_akun') is-invalid @enderror" name="server_akun" value="{{old('server_akun')?old('server_akun'):$order->server_akun}}">
        </div>
        @error('server_akun')
            <div class="alert alert-danger">Server Akun wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Metode Pembayaran</label>
            <select name="payment_id" id="" class="form-control @error('payment_id') is-invalid @enderror rounded">
                <option value="">-- pilih user --</option>
                
                @foreach ($payments as $item)
                    <option value="{{$item->id}}" <?php if($order->payment_id == $item->id) echo 'selected' ?>>{{$item->nama}}</option>
                @endforeach
            </select>
        </div>
        @error('payment_id')
            <div class="alert alert-danger">Pilih Denom Diamond</div>
        @enderror

        <div class="form-group">
            <label>Nomor WhatsApp</label>
            <input type="number" class="form-control @error('nomor_wa') is-invalid @enderror" name="nomor_wa" value="{{old('nomor_wa')?old('nomor_wa'):$order->nomor_wa}}">
        </div>
        @error('nomor_wa')
            <div class="alert alert-danger">Nomor WhatsApp wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Nomor Invoice</label>
            <input type="text" class="form-control @error('') is-invalid @enderror" name="" value="{{substr($order->bukti_pembayaran, 0, 24)}}" readonly>
        </div>

        <div class="form-group row">
            <label class="col-12">Bukti Pembayaran</label>
            <img src="{{asset('/assets/bukti-pembayaran/' . $order->bukti_pembayaran)}}" alt="" width="250px" class="mb-3">
        </div>

        <button type="submit" class="btn btn-primary mb-4">Submit</button>
    </form>
@endsection
