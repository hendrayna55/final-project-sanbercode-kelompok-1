@extends('admin.master_admin')

@section('title')
    Edit Metode Pembayaran
@endsection

@section('content')
    <form action="{{url('/metode-pembayaran/' . $method->id)}}" method="post" class="m-3" enctype="multipart/form-data">
        @csrf
        @method('put')
        
        <div class="form-group">
            <label>Nama Metode Pembayaran</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{old('nama')?old('nama'):$method->nama}}">
        </div>
        @error('nama')
            <div class="alert alert-danger">Nama Metode Pembayaran wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Nomor Rekening</label>
            <input type="text" class="form-control @error('no_rekening') is-invalid @enderror" name="no_rekening" value="{{old('no_rekening')?old('no_rekening'):$method->no_rekening}}">
        </div>
        @error('no_rekening')
            <div class="alert alert-danger">Nomor Rekening wajib diisi</div>
        @enderror

        <div class="form-group row">
            <label class="col-12">Logo</label>
            <img src="{{asset('/assets/logo-metode-pembayaran/' . $method->logo)}}" alt="" width="150px" class="mb-3">
            <input type="file" class="form-control-file @error('logo') is-invalid @enderror" name="logo">
        </div>
        @error('logo')
            <div class="alert alert-danger">Logo Metode Pembayaran wajib diisi</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
