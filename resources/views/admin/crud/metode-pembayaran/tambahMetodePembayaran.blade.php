@extends('admin.master_admin')

@section('title')
    Tambah Metode Pembayaran
@endsection

@section('content')
    <form action="{{url('/metode-pembayaran')}}" method="post" class="m-3" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Nama Metode Pembayaran</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{old('nama')}}">
        </div>
        @error('nama')
            <div class="alert alert-danger">Nama Metode Pembayaran wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Nomor Rekening</label>
            <input type="text" class="form-control @error('no_rekening') is-invalid @enderror" name="no_rekening" value="{{old('no_rekening')}}">
        </div>
        @error('no_rekening')
            <div class="alert alert-danger">Nomor Rekening wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Logo</label>
            <input type="file" class="form-control-file @error('logo') is-invalid @enderror" name="logo">
        </div>
        @error('logo')
            <div class="alert alert-danger">Logo Metode Pembayaran wajib diisi</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
