@extends('admin.master_admin')

@section('title')
    Tambah Nominal
@endsection

@section('content')
    <form action="/nominal" method="post" class="m-3">
        @csrf
        <div class="form-group">
            <label>Nominal Produk</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">Nominal produk wajib diisi</div>
        @enderror

        <div class="form-group">
            <label>Harga</label>
            <input type="number" class="form-control @error('harga') is-invalid @enderror" name="harga">
        </div>
        @error('harga')
            <div class="alert alert-danger">Harga produk wajib diisi</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
