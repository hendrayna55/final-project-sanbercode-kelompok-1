@extends('admin.master_admin')

@section('title')
    Data Order
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endpush

@push('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{asset('AdminLTE')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

    <!-- Page specific script -->
    <script>
    $(function () {
        $("#example1").DataTable({
        "responsive": false, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print", "colvis"] , "scrollX" : true
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        });
    });
    </script>
@endpush

@push('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('AdminLTE')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('AdminLTE')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('AdminLTE')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="{{url('/order/create')}}" class="btn btn-primary btn-sm m-3 float-right">Tambah Order</a>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-striped table-sm nowrap" id="example1">
                <thead>
                    <tr class="text-center bg-info">
                        <th class="text-center align-middle">Tanggal Order</th>
                        <th class="text-center align-middle">User</th>
                        <th class="text-center align-middle">Denom Diamond</th>
                        <th class="text-center align-middle">Nickname</th>
                        <th class="text-center align-middle">ID-Server</th>
                        <th class="text-center align-middle">Nomor Invoice</th>
                        <th class="text-center align-middle">Payment</th>
                        <th class="text-center align-middle">Bukti Pembayaran</th>
                        <th class="text-center align-middle">Nomor WA</th>
                        <th class="text-center align-middle">Status</th>
                        <th class="text-center align-middle">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr class="text-center">
                            <td class="text-center align-middle">{{$order->created_at}}</td>
                            <td class="text-center align-middle">{{$order->user->username}}</td>
                            <td class="text-center align-middle">{{$order->produk->nama}} <i class="fas fa-gem"></i></td>
                            <td class="text-center align-middle">{{$order->nickname}}</td>
                            <td class="text-center align-middle">{{$order->id_akun}} ({{$order->server_akun}})</td>
                            <td class="text-center align-middle">{{ substr($order->bukti_pembayaran, 0, 24)}}</td>
                            <td class="text-center align-middle">{{$order->payment->nama}}</td>
                            <td class="text-center align-middle">
                                <button class="btn btn-sm btn-success rounded" data-toggle="modal" data-target="#modal-lihat{{$order->id}}">
                                    Lihat
                                </button>
                            </td>
                            <td class="text-center align-middle">{{$order->nomor_wa}}</td>
                            @if ($order->status == 'Done')
                                <td class="text-center align-middle text-success">
                                    <button class="btn btn-sm btn-success rounded" data-toggle="modal" data-target="#modal-status{{$order->id}}">
                                        Success
                                    </button>
                                </td>
                            @elseif($order->status == 'Cancel')
                                <td class="text-center align-middle">
                                    <button class="btn btn-sm btn-danger rounded" data-toggle="modal" data-target="#modal-status{{$order->id}}">
                                        Cancel
                                    </button>
                                </td>
                            @else
                                <td class="text-center align-middle">
                                    <button class="btn btn-sm btn-warning rounded" data-toggle="modal" data-target="#modal-status{{$order->id}}">
                                        Pending
                                    </button>
                                </td>
                            @endif
                            <td class="text-center align-middle">
                                <a href="{{url('/order/' . $order->id . '/edit')}}" class="btn btn-sm btn-success">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-hapus{{$order->id}}">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
    
                        <!-- Modal Lihat Bukti Pembayaran -->
                        <div class="modal fade" id="modal-lihat{{$order->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-info">
                                        <h4 class="modal-title">Bukti Pembayaran</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p class="text-center">
                                            {{$order->nickname}}<br>
                                            {{$order->id_akun}} ({{$order->server_akun}})<br>
                                            Denom : {{$order->produk->nama}} <i class="fas fa-gem"></i>
                                        </p>
                                        <div class="text-center">
                                            <img src="{{asset('/assets/bukti-pembayaran/' . $order->bukti_pembayaran)}}" alt="" width="450px">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-info float-right" data-dismiss="modal">Tutup</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        <!-- Modal Ganti Status -->
                        <div class="modal fade" id="modal-status{{$order->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{url('/order/' . $order->id)}}" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="modal-header bg-info">
                                            <h4 class="modal-title">Status Order</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p class="text-center">
                                                {{$order->nickname}}<br>
                                                {{$order->id_akun}} ({{$order->server_akun}})<br>
                                                Denom : {{$order->produk->nama}} <i class="fas fa-gem"></i>
    
                                                <select name="status" id="" class="mt-2 form-control @error('status') is-invalid @enderror">
                                                    <option value="">-- pilih status --</option>
    
                                                    <option value="Done" <?php if($order->status == "Done") echo 'selected' ?>>
                                                        Done
                                                    </option>
                                                    <option value="Cancel" <?php if($order->status == "Cancel") echo 'selected' ?>>
                                                        Cancel
                                                    </option>
                                                    <option value="Process" <?php if($order->status == "Process") echo 'selected' ?>>
                                                        Process
                                                    </option>
                                                </select>
    
                                                <div class="d-none">
                                                    <input name="users_id" value="{{$order->users_id}}">
                                                    <input name="nickname" value="{{$order->nickname}}">
                                                    <input name="id_akun" value="{{$order->id_akun}}">
                                                    <input name="server_akun" value="{{$order->server_akun}}">
                                                    <input name="nomor_wa" value="{{$order->nomor_wa}}">
                                                    <input name="diamond_idserver_id" value="{{$order->diamond_idserver_id}}">
                                                    <input name="payment_id" value="{{$order->payment_id}}">
                                                </div>
                                            </p>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-info float-right" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-success float-right">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        <!-- Modal Hapus -->
                        <div class="modal fade" id="modal-hapus{{$order->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title">Hapus Data Order</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p class="">
                                            Nickname : {{$order->nickname}}<br>
                                            ID : {{$order->id_akun}} ({{$order->server_akun}})<br>
                                            Denom : {{$order->produk->nama}} <i class="fas fa-gem"></i><br><br>
                                            Apakah anda yakin untuk menghapus data ini?
                                        </p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        <form action="{{url('/order/' . $order->id)}}" method="post" class="">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm mr-2" data-toggle="modal" data-target="#modal-default">Hapus</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    @endforeach
                    
                </tbody>
            
            </table>
    
        </div>
        
    </div>
@endsection

