@extends('admin.master_admin')

@section('title')
    Metode Pembayaran
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endpush

@push('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{asset('AdminLTE')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

    <!-- Page specific script -->
    <script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print", "colvis"] ,
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        });
    });
    </script>
@endpush

@push('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('AdminLTE')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('AdminLTE')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('AdminLTE')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="/metode-pembayaran/create" class="btn btn-primary btn-sm m-3 float-right">Tambah Metode Pembayaran</a>
        </div>
        <div class="col-12">
            <table class="table table-bordered table-striped table-sm nowrap" id="example1">
                <thead>
                    <tr class="text-center bg-info">
                        <th class="text-center align-middle">Metode Pembayaran</th>
                        <th class="text-center align-middle">Nomor Rekening</th>
                        <th class="text-center align-middle">Logo</th>
                        <th class="text-center align-middle">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($methods as $method)
                        <tr class="text-center">
                            <td class="text-center align-middle">{{$method->nama}}</td>
                            <td class="text-center align-middle">{{$method->no_rekening}}</td>
                            <td class="text-center align-middle">
                                <img src="{{asset('/assets/logo-metode-pembayaran/' . $method->logo)}}" alt="" width="100px">
                            </td>
                            <td class="text-center align-middle">
                                <a href="{{url('/metode-pembayaran/' . $method->id . '/edit')}}" class="btn btn-sm btn-success">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-hapus{{$method->id}}">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
    
                        <!-- Modal Hapus -->
                        <div class="modal fade" id="modal-hapus{{$method->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title">Hapus Data Produk</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p class="">
                                            Nama Produk : {{$method->nama}} <i class="fas fa-gem"></i><br>
                                            Harga : Rp{{$method->no_rekening}}<br><br>
                                            Apakah anda yakin untuk menghapus data ini?
                                        </p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        <form action="{{url('/nominal/' . $method->id)}}" method="post" class="">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm mr-2" data-toggle="modal" data-target="#modal-default">Hapus</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    @endforeach
                    
                </tbody>
            
            </table>
    
        </div>
        
    </div>
@endsection

