@extends('admin.master_admin')

@section('title')
    User
@endsection

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endpush

@push('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{asset('AdminLTE')}}/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/jszip/jszip.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/pdfmake/pdfmake.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/pdfmake/vfs_fonts.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="{{asset('AdminLTE')}}/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>

    <!-- Page specific script -->
    <script>
    $(function () {
        $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["excel", "pdf", "print", "colvis"] ,
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        });
    });
    </script>
@endpush

@push('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('AdminLTE')}}/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('AdminLTE')}}/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="{{asset('AdminLTE')}}/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endpush

@section('content')
    <div class="row">
        <div class="col-12">
            <a href="{{url('/user/create')}}" class="btn btn-primary btn-sm m-3 float-right">Tambah User</a>
        </div>
        
        <div class="col-12">
            <table class="table table-bordered table-striped table-sm nowrap" id="example1">
                <thead>
                    <tr class="text-center bg-info">
                        <th class="text-center align-middle">Nama</th>
                        <th class="text-center align-middle">Username</th>
                        <th class="text-center align-middle">Email</th>
                        <th class="text-center align-middle">Role</th>
                        <th class="text-center align-middle">No. WhatsApp</th>
                        <th class="text-center align-middle">Status</th>
                        <th class="text-center align-middle">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($users as $item)
                        <tr class="text-center">
                            <td class="text-center align-middle">{{$item->profile->nama}}</td>
                            <td class="text-center align-middle">{{$item->username}}</td>
                            <td class="text-center align-middle">{{$item->email}}</td>
                            <td class="text-center align-middle">{{$item->role->name}}</td>
                            @if ($item->profile->nomor_wa == null)
                                <td class="text-center align-middle text-danger">
                                    Belum Update
                                </td>
                            @else
                                <td class="text-center align-middle">{{$item->profile->nomor_wa}}</td>
                            @endif
                            @if ($item->verified_status == 1)
                                <td class="text-center align-middle text-success">
                                    Terverifikasi
                                </td>
                            @else
                                <td class="text-center align-middle text-danger">
                                    <div class="btn btn-sm text-danger" data-toggle="modal" data-target="#modal-verifikasi{{$item->id}}">Belum Verifikasi</div>
                                </td>
                            @endif
                            
                            <td class="text-center align-middle">
                                <a href="{{url('/user/' . $item->id . '/edit')}}" class="btn btn-sm btn-success">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#modal-hapus{{$item->id}}">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>

                        <!-- Modal Verifikasi -->
                        <div class="modal fade" id="modal-verifikasi{{$item->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <form action="{{url('/user/' . $item->id)}}" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="modal-header bg-success">
                                            <h4 class="modal-title">Verifikasi User</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p class="text-center">
                                                Username : {{$item->username}}<br>
                                                Email : {{$item->email}}<br>
    
                                                <select name="verified_status" id="" class="mt-2 form-control @error('verified_status') is-invalid @enderror">    
                                                    <option value="1" <?php if($item->verified_status == 1) echo 'selected' ?>>
                                                        Terverifikasi
                                                    </option>
                                                    <option value="0" <?php if($item->status == 0) echo 'selected' ?>>
                                                        Belum Verifikasi
                                                    </option>
                                                </select>
    
                                                <div class="d-none">
                                                    <input name="username" value="{{$item->username}}">
                                                    <input name="email" value="{{$item->email}}">
                                                    <input name="role_id" value="{{$item->role_id}}">
                                                </div>
                                            </p>
                                        </div>
                                        <div class="modal-footer justify-content-between">
                                            <button type="button" class="btn btn-info float-right" data-dismiss="modal">Tutup</button>
                                            <button type="submit" class="btn btn-success float-right">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->

                        <!-- Modal Hapus -->
                        <div class="modal fade" id="modal-hapus{{$item->id}}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-danger">
                                        <h4 class="modal-title">Hapus Data User</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p class="">
                                            Username : {{$item->username}}<br>
                                            Email : {{$item->email}}<br><br>
                                            Apakah anda yakin untuk menghapus data ini?
                                        </p>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                                        <form action="{{url('/user/' . $item->id)}}" method="post" class="">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm mr-2" data-toggle="modal" data-target="#modal-default">Hapus</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                    @endforeach
                    
                </tbody>
            
            </table>
    
        </div>
        
    </div>
@endsection

