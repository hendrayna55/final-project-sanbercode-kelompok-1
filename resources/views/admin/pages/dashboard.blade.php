@extends('admin.master_admin')

@section('title')
    Dashboard
@endsection

@section('content')
    <p>Selamat datang username</p>
    <div class="row justify-content-center">
        <div class="col-lg-3 col-sm-6">
            <div class="info-box">
                <span class="info-box-icon bg-info elevation-1">
                    <i class="far fa-user-circle"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">User</span>
                    <span class="info-box-number">{{($users)}} User</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-lg-3 col-sm-6">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-warning elevation-1">
                    <i class="far fa-user"></i>
                </span>

                <div class="info-box-content">
                    <span class="info-box-text">Akun MLBB</span>
                    <span class="info-box-number">{{$akuns}} Akun</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-lg-3 col-sm-6">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1">
                    <i class="fas fa-money-bill"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Produk</span>
                    <span class="info-box-number">{{$produks}} Produk</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix hidden-md-up"></div>

        <div class="col-lg-3 col-sm-6">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-danger elevation-1">
                    <i class="fas fa-wallet"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Metode Pembayaran</span>
                    <span class="info-box-number">{{$payments}} Metode</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <div class="col-lg-3 col-sm-6">
            <div class="info-box mb-3">
                <span class="info-box-icon bg-success elevation-1">
                    <i class="fas fa-credit-card"></i>
                </span>
                <div class="info-box-content">
                    <span class="info-box-text">Orderan</span>
                    <span class="info-box-number">{{$orders}} Order</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        
    </div>
    <!-- /.row -->
@endsection
