@extends('layouts.master')

@push('style')
    <!-- Google Font: Source Sans Pro -->
    {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('/AdminLTE/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('/AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('/AdminLTE/dist/css/adminlte.min.css')}}"> --}}
@endpush

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-4 login-box">
        <!-- /.login-logo -->
            <div class="card card-outline bg-dark rounded card-primary">
                <div class="card-header  text-center">
                    <a href="{{url('/')}}" class="h1"><b>Kenji</b> Store</a>
                </div>
                <div class="card-body">
                    <p class="login-box-msg text-white text-center">Login untuk masuk ke portal Top Up</p>

                    <form action="{{ route('login') }}" method="post">
                        @csrf
                        <div class="input-group mb-3">
                            <input name="email" type="text" class="form-control" placeholder="Email">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                        </div>

                        <div class="input-group mb-3">
                            <input name="password" type="password" class="form-control" placeholder="Password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-8">
                                
                            </div>
                            <!-- /.col -->
                            <div class="col-4">
                                <button type="submit" class="btn btn-primary btn-block rounded">Login</button>
                            </div>
                            <!-- /.col -->
                        </div>
                    </form>

                    <p class="mb-1">
                        {{-- <a href="forgot-password.html">Lupa Password</a> --}}
                    </p>
                    <p class="mb-0">
                        <a href="{{url('/register')}}" class="text-center">Register</a>
                    </p>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- /.login-box -->
    </div>
@endsection