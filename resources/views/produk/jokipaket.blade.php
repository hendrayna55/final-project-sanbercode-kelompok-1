@extends('layouts.master')

@section('content')
    <div class="container-fluid px-xl-5">
        <div class="px-xl-5">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-12">
                    <div class="card m-auto bg-dark rounded" style="width: 100%;">
                        <div class="px-5 py-3">
                            <img class="w-full rounded" src="{{asset('assets/img/bened.png')}}" alt="Card image cap" style="width:100%;">
                            
                        </div>
                        <div class="card-body text-white">
                            <h3 class="text-white">Joki Rank Paket</h3>
                            <h6 class="text-body text-secondary">
                                Jasa Joki (Up Rank)<br>
                                <span class="active text-warning">Open 24 Jam</span>
                            </h6>
                            <p class="card-text">
                                Orderan Joki Di Cek Jam 13.00 - 23.00 WIB
                            </p>
                            <p class="card-text">
                                Cara Order :
                            </p>
                            <ol class="">
                                <li>Lengkapi Data Joki Dengan Teliti!</li>
                                <li>Pilih Jenis Rank</li>
                                <li>Masukkan Jumlah Per (Star/Point)</li>
                                <li>Pilih Metode Pembayaran</li>
                                <li>Tulis nomor WhatsApp yg benar</li>
                                <li>Lakukan pembayaran sesuai metode yang kamu pilih dan upload bukti</li>
                                <li>Klik Order Now</li>
                                <li>Orderan Joki akan segera di proses setelah pembayaran berhasil</li>
                            </ol>
                            <p class="text-warning" style="font-size:15px;">
                                Estimasi Jasa Joki Kita Usahakan Secepatnya
                            </p>
                            <p class=" text-warning text-sm" style="font-size:15px;">
                                Minimal 12 Jam - Maximal 1x24 Jam
                            </p>
                        </div>
                    </div>
                </div>
    
                <div class="col-lg-8 col-sm-12">
                    <div class="card-kanan">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">Lengkapi Data</h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <input type="text" class="form-control rounded" placeholder="Masukkan Email/No HP">
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control rounded" placeholder="Masukkan Password">
                                    </div>
                                    <div class="col-6 pt-4">
                                        <input type="text" class="form-control rounded" placeholder="Masukkan Nickname">
                                    </div>
                                    <div class="col-6 pt-4">
                                        <input type="text" class="form-control rounded" placeholder="Masukkan ID Akun">
                                    </div>
                                    <div class="col-6 pt-4">
                                        <input type="text" class="form-control rounded" placeholder="Catatan Untuk Admin">
                                    </div>
                                    <div class="col-6 pt-4">
                                        <select class="select dropdown form-control rounded">
                                            <option value="1" class="dropdown-header">Login Method</option>
                                            <option value="1">Moonton (Rekomendasi)</option>
                                            <option value="1">VK (Rekomendasi)</option>
                                            <option value="2">Facebook</option>
                                            <option value="3">TikTok</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="pt-3">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">Pilih Joki Rank Per Star</h5>
                            <div class="card-body">
                                <div class="row justify-content-center text-white">
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="pt-3">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">Masukkan Jumlah (Star)</h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <input type="text" class="form-control rounded" value="1">
                                        <label class="text-white pt-1">** Minimal Order Untuk Rank 3 Star. Jika Kurang Dari Minimal order maka uang akan hangus.</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
    
                    <div class="pt-3">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">Pilih Metode Pembayaran</h5>
                            <div class="card-body">
                                <div class="row justify-content-center text-white">
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" src="{{asset('assets/img')}}/DANA.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">DANA</h6>
                                                <p class="">0815-2194-1914</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height:60px;" src="{{asset('assets/img')}}/GOPAY.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Go-Pay</h6>
                                                <p class="">0815-2194-1914</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" src="{{asset('assets/img')}}/SHOPEEPAY.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Shopee Pay</h6>
                                                <p class="">0815-2194-1914</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height:60px;" src="{{asset('assets/img')}}/OVO.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">OVO</h6>
                                                <p class="">0815-2194-1914</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" src="{{asset('assets/img')}}/BRI.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Bank BRI</h6>
                                                <p class="">2202-0100-5456-501</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height: 65px;" src="{{asset('assets/img')}}/JAGO.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Bank Jago</h6>
                                                <p class="">1028-4459-9919</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height: 65px;" src="{{asset('assets/img')}}/SEABANK.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">SeaBank</h6>
                                                <p class="">9014-7704-4407</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height: 65px;" src="{{asset('assets/img')}}/BSI.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Bank BSI</h6>
                                                <p class="">1048603471</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pt-3 card-kanan">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">Nomor WhatsApp</h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="text-white">Nomor WhatsApp</label>
                                            <input type="email" class="form-control rounded" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Nomor WhatsApp Kamu">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="text-white">Bukti Transfer</label>
                                            <input type="file" class="form-control rounded" id="exampleInputEmail1" aria-describedby="emailHelp">
                                        </div>
                                    </div>
                                    <div class="pt-4 col-8">
                                        <div class="pt-2 form-group text-center">
                                            <button type="submit" class="bg-secondary form-control rounded">
                                                <i class="bi bi-cart-fill">
                                                    <span class="text-uppercase text-black"> Order Now</span>
                                                </i>
                                            </button>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
            </div>

        </div>
    </div>
@endsection