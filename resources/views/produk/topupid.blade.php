@extends('layouts.master')

@push('script')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

@endpush
@section('content')
    
    <div class="container-fluid px-xl-5">
        <div class="px-xl-5">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-12">
                    <div class="card m-auto bg-dark rounded" style="width: 100%;">
                        <div class="px-5 py-3">
                            <img class="w-full" src="{{asset('assets/img/diamond-id-no-bg.png')}}" alt="Card image cap" style="width:100%;">
                            
                        </div>
                        <div class="card-body text-white">
                            <h3 class="text-white">Top Up Diamond</h3>
                            <h6 class="text-body">
                                Proses Otomatis<br>
                                <span class="active text-warning">Open 24 Jam</span>
                            </h6>
                            <p class="card-text">
                                Top Up Diamond Mobile Legends Resmi Moonton 100% Legal, Murah, Aman Dan Terpercaya. Cara Top Up Diamond Mobile Legends :
                            </p>
                            <ol class="">
                                <li>Masukkan ID (SERVER)</li>
                                <li>Pilih Nominal Diamond</li>
                                <li>Pilih Metode Pembayaran</li>
                                <li>Tulis nomor WhatsApp yg benar</li>
                                <li>Lakukan pembayaran sesuai metode yang kamu pilih dan upload bukti</li>
                                <li>Klik Order Now</li>
                                <li>Diamond Akan Masuk ke akun Anda</li>
                            </ol>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-sm-12">
                    <form action="{{url('/order-diamond-idserver')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-kanan">
                            <div class="card m-auto bg-dark rounded">
                                <h5 class="card-header text-white">Lengkapi Data</h5>
                                <div class="card-body">
                                    {{-- <select name="nickname" id="" class="form-control @error('user_id') is-invalid @enderror rounded my-1">
                                        <option value="">-- pilih akun --</option>
                                        @foreach ($akuns as $akun)
                                            <option value="{{$akun->nickname}}">{{$akun->nickname}} ({{$akun->id_akun}} - {{$akun->server_akun}})</option>
                                        @endforeach
                                    </select> --}}
                                    <div class="row">
                                        <div class="col-lg-12 col-sm-12 my-1">
                                            <input name="nickname" type="text" class="form-control @error('nickname') is-invalid @enderror rounded" placeholder="Masukkan Nickname Akun" value="{{old('nickname')}}">
                                            
                                            @error('nickname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Nickname wajib diisi!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-6 col-sm-12 my-1">
                                            <input name="id_akun" type="text" class="form-control @error('id_akun') is-invalid @enderror rounded" placeholder="Masukkan ID Akun" value="{{old('id_akun')}}">
                                            
                                            @error('id_akun')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>ID Akun wajib diisi!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-lg-6 col-sm-12 my-1">
                                            <input name="server_akun" type="text" class="form-control @error('server_akun') is-invalid @enderror rounded" placeholder="Masukkan Server Akun" value="{{old('server_akun')}}">
                                            
                                            @error('server_akun')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Server Akun wajib diisi!</strong>
                                                </span>
                                            @enderror
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
        
                        <div class="pt-3">
                            <div class="card m-auto bg-dark rounded">
                                <h5 class="card-header text-white">Pilih Nominal Top Up Diamond</h5>
                                <div class="card-body">
                                    <div class="row justify-content-center text-white">

                                        {{-- @foreach ($produks as $produk)
                                            <div class="col-lg-4 col-sm-6 px-2 text-center">
                                                <div class="rounded produk">
                                                    <p class="p-2">{{$produk->nama}} Diamonds<br><small><i>Rp. {{$produk->harga}}</i></small></p>
                                                </div>
                                            </div>
                                        @endforeach --}}

                                        {{-- <div class="col-lg-12 col-sm-12 my-1">
                                            <select name="diamond_idserver_id" id="" class="form-control @error('diamond_idserver_id') is-invalid @enderror rounded my-1">
                                                <option value="">-- pilih denom diamond --</option>
                                                @foreach ($produks as $produk)
                                                    <option value="{{$produk->id}}">{{$produk->nama}} Diamond - Rp. {{$produk->harga}}</option>
                                                @endforeach

                                            </select>
                                            @error('diamond_idserver_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Pilih Produk!</strong>
                                                </span>
                                            @enderror
                                        </div> --}}

                                        @foreach ($produks as $produk)
                                            <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                                <div class="rounded produk klikproduk" onclick="highlightProduk(this, {{$produk->id}})">
                                                    
                                                    <div class="flex-fill text-center">
                                                        <p class="p-2">{{$produk->nama}} Diamonds<br><small><i>Rp. {{$produk->harga}}</i></small></p>
                                                        <input type="radio" id="diamond_idserver_{{$produk->id}}" name="diamond_idserver_id" value="{{$produk->id}}" style="display: none;" class="form-control @error('diamond_idserver_id') is-invalid @enderror">
                                                        <label for="diamond_idserver_{{$produk->id}}" class="radio-label d-none"></label>
                                                        @error('diamond_idserver_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>Pilih Produk!</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
        
                        <div class="pt-3">
                            <div class="card m-auto bg-dark rounded">
                                <h5 class="card-header text-white">Pilih Metode Pembayaran</h5>
                                <div class="card-body">
                                    <div class="row justify-content-center text-white">

                                        {{-- <div class="col-lg-12 col-sm-12 my-1">
                                            <select name="payment_id" id="" class="form-control @error('payment_id') is-invalid @enderror rounded my-1">

                                                <option value="">-- pilih metode pembayaran --</option>
                                                
                                                @foreach ($payments as $payment)
                                                    <option value="{{$payment->id}}">{{$payment->nama}} - {{$payment->no_rekening}}</option>
                                                @endforeach

                                            </select>
                                            @error('payment_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Pilih Metode Pembayaran!</strong>
                                                </span>
                                            @enderror
                                        </div> --}}

                                        @foreach ($payments as $payment)
                                            <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                                <div class="produk d-flex align-items-center mb-4 rounded clickable" onclick="highlightPayment(this, {{$payment->id}})">
                                                    <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                        <img class="pl-3 img-fluid my-auto" src="{{asset('assets/logo-metode-pembayaran/' . $payment->logo)}}" alt="" width="80px">
                                                    </div>
                                                    <div class="flex-fill text-center">
                                                        <h6 class="text-center">{{$payment->nama}}</h6>
                                                        <p class="">{{$payment->no_rekening}}</p>
                                                        <input type="radio" id="payment_{{$payment->id}}" name="payment_id" value="{{$payment->id}}" style="display: none;" class="form-control @error('payment_id') is-invalid @enderror">
                                                        <label for="payment_{{$payment->id}}" class="d-none radio-label"></label>
                                                        @error('payment_id')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>Pilih Metode Pembayaran!</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="pt-3 card-kanan">
                            <div class="card m-auto bg-dark rounded">
                                <h5 class="card-header text-white">Nomor WhatsApp</h5>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="nomor_wa" class="text-white">Nomor WhatsApp</label>
                                                <input type="text" class="form-control @error('nomor_wa') is-invalid @enderror rounded" name="nomor_wa" placeholder="Masukkan Nomor WhatsApp Kamu" value="{{old('nomor_wa')}}">
                                                <input type="text" class="d-none form-control @error('status') is-invalid @enderror rounded" name="status" placeholder="Masukkan Nomor WhatsApp Kamu" value="Process">

                                                @error('nomor_wa')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>Isi Nomor WhatsApp!</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="bukti_pembayaran" class="text-white">Bukti Transfer</label>
                                                <input type="file" class="form-control-file @error('bukti_pembayaran') is-invalid @enderror rounded" name="bukti_pembayaran">

                                                @error('bukti_pembayaran')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="pt-4 col-6">
                                            <div class="pt-2 form-group text-center">
                                                <button type="submit" class="bg-secondary form-control rounded">
                                                    <i class="bi bi-cart-fill">
                                                        <span class="text-uppercase text-black"> Order Now</span>
                                                    </i>
                                                </button>
                                                
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    
                </div>
        
            </div>

        </div>
    </div>

    <style>
        /* Style untuk box saat dipilih */
        .highlight {
            box-shadow: 0 0 10px #fff;
        }
    </style>

    <script>
        function highlightPayment(element, paymentId) {
            // Menghapus kelas highlight dari semua elemen
            var allElements = document.getElementsByClassName('clickable');
            for (var i = 0; i < allElements.length; i++) {
                allElements[i].classList.remove('highlight');
            }
            
            // Menambahkan kelas highlight pada elemen yang diklik
            element.classList.add('highlight');

            var radio = document.getElementById("payment_" + paymentId);
            radio.checked = true;
        }
    </script>

    <script>
        function highlightProduk(element, produkId) {
            // Menghapus kelas highlight dari semua elemen
            var allElements = document.getElementsByClassName('klikproduk');
            for (var i = 0; i < allElements.length; i++) {
                allElements[i].classList.remove('highlight');
            }
            
            // Menambahkan kelas highlight pada elemen yang diklik
            element.classList.add('highlight');

            var radio = document.getElementById("diamond_idserver_" + produkId);
            radio.checked = true;
        }
    </script>
    
@endsection