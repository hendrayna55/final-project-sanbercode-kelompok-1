@extends('layouts.master')

@section('content')
    <div class="container-fluid px-xl-5">
        <div class="px-xl-5">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-sm-12">
                    <div class="card m-auto bg-dark rounded" style="width: 100%;">
                        <div class="px-5 py-3">
                            <img class="w-full" src="{{asset('assets/img/diamond-chest-no-bg.png')}}" alt="Card image cap" style="width:100%;">
                            
                        </div>
                        <div class="card-body text-white">
                            <h3 class="text-white">Top Up Diamond Vilog</h3>
                            <h6 class="text-body text-secondary">
                                Proses Manual<br>
                                <span class="active text-warning">Open 24 Jam</span>
                            </h6>
                            <p class="card-text">
                                Top Up Diamond Mobile Legends Resmi Moonton 100% Legal, Murah, Aman Dan Terpercaya. Cara Top Up Diamond Mobile Legends :
                            </p>
                            <ol class="">
                                <li>Lengkapi data login dengan teliti!</li>
                                <li>Pilih Nominal Diamond</li>
                                <li>Pilih Metode Pembayaran</li>
                                <li>Tulis nomor WhatsApp yg benar</li>
                                <li>Lakukan pembayaran sesuai metode yang kamu pilih dan upload bukti</li>
                                <li>Klik Order Now</li>
                                <li>Orderan vilog akan segera diproses setelah pembayaran berhasil</li>
                            </ol>
                            <p class="text-warning" style="font-size:15px;">
                                Estimasi Proses Top Up Vilog Kita Usahakan Secepatnya
                            </p>
                            <p class=" text-warning text-sm" style="font-size:15px;">
                                Minimal 2 Jam - Maximal 1x24 Jam
                            </p>
                            <p class=" text-warning text-sm" style="font-size:15px;">
                                **Catatan Penting**<br>
                                <ol class="text-warning">
                                    <li>Buyer yang sudah setup = Orderan max 30 menit proses</li>
                                    <li>Putuskan All Device = SETUP ULANG</li>
                                    <li>Legal with invoice & Data aman 100%</li>
                                    <li>Matikan Verifikasi 2 Langkah</li>
                                    <li>Dilarang keras untuk tabrak login</li>
                                </ol>
                            </p>
                        </div>
                    </div>
                </div>
    
                <div class="col-lg-8 col-sm-12">
                    <div class="card-kanan">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">Lengkapi Data</h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <input type="text" class="form-control rounded" placeholder="Masukkan Email/No HP">
                                    </div>
                                    <div class="col-6">
                                        <input type="text" class="form-control rounded" placeholder="Masukkan Password">
                                    </div>
                                    <div class="col-6 pt-4">
                                        <input type="text" class="form-control rounded" placeholder="Masukkan Nickname">
                                    </div>
                                    <div class="col-6 pt-4">
                                        <input type="text" class="form-control rounded" placeholder="Masukkan ID Akun">
                                    </div>
                                    <div class="col-6 pt-4">
                                        <input type="text" class="form-control rounded" placeholder="Catatan Untuk Admin">
                                    </div>
                                    <div class="col-6 pt-4">
                                        <select class="select dropdown form-control rounded">
                                            <option value="1" class="dropdown-header">Login Method</option>
                                            <option value="1">Moonton (Rekomendasi)</option>
                                            <option value="1">VK (Rekomendasi)</option>
                                            <option value="2">Facebook</option>
                                            <option value="3">TikTok</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="pt-3">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">Pilih Nominal Top Up Diamond Vilog</h5>
                            <div class="card-body">
                                <div class="row justify-content-center text-white">
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 px-2 text-center">
                                        <div class="rounded produk">
                                            <p class="p-2">86 (78+8) Diamonds<br><small><i>Rp20.000</i></small></p>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
    
                    <div class="pt-3">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">Pilih Metode Pembayaran</h5>
                            <div class="card-body">
                                <div class="row justify-content-center text-white">
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" src="{{asset('assets/img')}}/DANA.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">DANA</h6>
                                                <p class="">0815-2194-1914</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height:60px;" src="{{asset('assets/img')}}/GOPAY.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Go-Pay</h6>
                                                <p class="">0815-2194-1914</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" src="{{asset('assets/img')}}/SHOPEEPAY.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Shopee Pay</h6>
                                                <p class="">0815-2194-1914</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height:60px;" src="{{asset('assets/img')}}/OVO.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">OVO</h6>
                                                <p class="">0815-2194-1914</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" src="{{asset('assets/img')}}/BRI.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Bank BRI</h6>
                                                <p class="">2202-0100-5456-501</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height: 65px;" src="{{asset('assets/img')}}/JAGO.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Bank Jago</h6>
                                                <p class="">1028-4459-9919</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height: 65px;" src="{{asset('assets/img')}}/SEABANK.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">SeaBank</h6>
                                                <p class="">9014-7704-4407</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-sm-6 pt-1 px-2">
                                        <div class="produk d-flex align-items-center mb-4 rounded">
                                            <div class="overflow-hidden justify-content-center align-items-center d-flex" style="width: 100px; height: 100px;">
                                                <img class="pl-3 img-fluid my-auto" style="height: 65px;" src="{{asset('assets/img')}}/BSI.png" alt="">
                                            </div>
                                            <div class="flex-fill text-center">
                                                <h6 class="text-center">Bank BSI</h6>
                                                <p class="">1048603471</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pt-3 card-kanan">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">Nomor WhatsApp</h5>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="text-white">Nomor WhatsApp</label>
                                            <input type="email" class="form-control rounded" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan Nomor WhatsApp Kamu">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1" class="text-white">Bukti Transfer</label>
                                            <input type="file" class="form-control rounded" id="exampleInputEmail1" aria-describedby="emailHelp">
                                        </div>
                                    </div>
                                    <div class="pt-4 col-8">
                                        <div class="pt-2 form-group text-center">
                                            <button type="submit" class="bg-secondary form-control rounded">
                                                <i class="bi bi-cart-fill">
                                                    <span class="text-uppercase text-black"> Order Now</span>
                                                </i>
                                            </button>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        
            </div>

        </div>
    </div>
@endsection