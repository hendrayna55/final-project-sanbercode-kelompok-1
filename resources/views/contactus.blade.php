@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="pt-4 col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-sm-12">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white">
                                Contact Us
                            </h5>
                            <div class="card-body">
                                <table class="table table-striped table-bordered">
                                    <tr class="text-center text-dark bg-light" style="background-color:#FFD333;">
                                        <th>Nama</th>
                                        <th>Platform</th>
                                        <th>Kontak</th>
                                    </tr>
                                    <tr class="text-white">
                                        <td>Kenji Matsuya</td>
                                        <td>WhatsApp</td>
                                        <td><a href="https://wa.me/6281521941914">+62 815-2194-1914</a></td>
                                    </tr>
                                    <tr class="text-white">
                                        <td>Kenji Matsuya</td>
                                        <td>Instagram</td>
                                        <td><a href="https://wa.me/6281521941914">@kenjistore.moba</a></td>
                                    </tr>
                                    <tr class="text-white">
                                        <td>Kenji Matsuya</td>
                                        <td>TikTok</td>
                                        <td><a href="https://wa.me/6281521941914">@inikenjijuga</a></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection