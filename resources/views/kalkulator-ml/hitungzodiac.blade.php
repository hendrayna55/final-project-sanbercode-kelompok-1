<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.head')
    <title>Hitung HP Zodiac</title>
</head>
<body style="background-color:#343a40">
    @include('layouts.navbar')

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <img class="mx-auto d-block" style="width:15%" src="{{asset('assets/img/Kenji-Store.png')}}" alt="">
            </div>
            <div class="pt-4 col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-6">
                        <div class="row text-white">
                            <div class="col-12 text-center">
                                <h3>Kalkulator Zodiac</h3>
                            </div>
                            <div class="col-12">
                                <p class="text-white">
                                    Kalkulator Zodiac ini berfungsi untuk mengetahui total maksimal diamond yang kamu butuhkan untuk mendapatkan skin Zodiac.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pt-2 row justify-content-center">
                    <div class="col-lg-5 col-sm-12">
                        <form>
                            <h5 class="text-white">Geser Sesuai Point Zodiac Anda</h4>
                            <div class="form-group">
                                <label for="inputwr" class="text-white pt-3">Star Point Anda : <span id='pointzodiacValue'> </span></label>
                                <input id="pointzodiac" type="range" class="form-control-range" min="0" max="99" id="formControlRange" id="myRange" onchange="if (!window.__cfRLUnblockHandlers) return false; show_value2(this.value)" data-cf-modified-b1bbf0c8980ad5c57da61c3f-="">
                                <p class="pt-3 text-white">
                                    Membutuhkan Maksimal : <span id="slider_value2" style="color:white;font-weight:bold;"></span><i class="fas fa-gem" style="color: #00c8c8"></i>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')
    @include('layouts.js')
    <script>
        let inputValue = document.getElementById('pointzodiac')
        console.log(inputValue);
        inputValue.addEventListener('change',(e)=>{
            document.getElementById('pointzodiacValue').innerText=e.target.value
        })
    </script>
</body>
</html>