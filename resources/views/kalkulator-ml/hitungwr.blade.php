@extends('layouts.master')

@push('script')
<script>
    // Variables
    const tMatch = document.querySelector("#tMatch");
    const tWr = document.querySelector("#tWr");
    const wrReq = document.querySelector("#wrReq");
    const hasil = document.querySelector("#hasil");
    const resultText = document.querySelector("#resultText");

    // Functions
    function res() {
        const resultNum = rumus(tMatch.value, tWr.value, wrReq.value);
        const text = `Kamu memerlukan sekitar <b>${resultNum}</b> win tanpa lose untuk mendapatkan win rate <b>${wrReq.value}%</b>`;
        resultText.innerHTML = text;
    }

    function rumus(tMatch, tWr, wrReq) {
        let tWin = tMatch * (tWr / 100);
        let tLose = tMatch - tWin;
        let sisaWr = 100 - wrReq;
        let wrResult = 100 / sisaWr;
        let seratusPersen = tLose * wrResult;
        let final = seratusPersen - tMatch;
        return Math.round(final);
    }

    // Main
    window.addEventListener("load", init);

    function init() {
        load();
        eventListener();
    }

    function load() {}

    function eventListener() {
        hasil.addEventListener("click", res);
    }
</script>
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <img class="mx-auto d-block" style="width:15%" src="{{asset('assets/img/Kenji-Store.png')}}" alt="">
            </div>
            <div class="pt-4 col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-sm-12">
                        <form action="">
                            <div class="row justify-content-center">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="inputwr" class="text-white">Total Pertandingan Anda</label>
                                        <input type="number" class="form-control rounded" id="tMatch" placeholder="Contoh : 268" autofocus>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="inputwr" class="text-white">Total Winrate Anda</label>
                                        <input type="number" class="form-control rounded" step="any" id="tWr" placeholder="Contoh : 56.4%">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="inputwr" class="text-white">Winrate Yang Anda Inginkan</label>
                                        <input type="number" class="form-control rounded" step="any" id="wrReq" placeholder="Contoh : 70%">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row text-center">
                                        <div class="col">
                                            <button type="button" id="hasil" class="btn btn-warning rounded">
                                                Lihat Hasil
                                            </button>
                                        </div>
                                        {{-- <div class="col-6">
                                            <a href="{{url('/topupid')}}" class="btn btn-warning rounded">
                                                Order Joki
                                            </a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
            </div>
            <div class="pt-3 col-lg-12 text-center">
                <p class="text-white"><span id="resultText" class="text-center d-block"> </span></p>
            </div>
        </div>
    </div>
@endsection