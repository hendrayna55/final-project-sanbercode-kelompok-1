@extends('layouts.master')

@push('script')
    <script>
        let inputValue = document.getElementById('pointmw')
        console.log(inputValue);
        inputValue.addEventListener('change',(e)=>{
            document.getElementById('pointmwValue').innerText=e.target.value
        })
    </script>
    <script type="b1bbf0c8980ad5c57da61c3f-text/javascript">
        function show_value2(x) {

            if (x < 196) {
                sisa_spin = 200 - x;
                jumlah_spin = Math.ceil(sisa_spin / 5);
                yz = jumlah_spin * 270;
            }
            if (x > 195) {
                sisa_spin = 200 - x;
                yz = sisa_spin * 60;
            }
            document.getElementById("slider_value2").innerHTML = yz;

        }
    </script>
    <script type="b1bbf0c8980ad5c57da61c3f-text/javascript">
        var slideCol = document.getElementById("myRange");
        var y = document.getElementById("f");
        y.innerHTML = slideCol.value;

        slideCol.oninput = function() {
            y.innerHTML = this.value;
        }
    </script>

@endpush

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <img class="mx-auto d-block" style="width:15%" src="{{asset('assets/img/Kenji-Store.png')}}" alt="">
            </div>
            <div class="pt-4 col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-6">
                        <div class="row text-white">
                            <div class="col-12 text-center">
                                <h3>Kalkulator Magic Wheel</h3>
                            </div>
                            <div class="col-12">
                                <p class="text-white">
                                    Kalkulator Magic Wheel berfungsi untuk mengetahui total maksimal diamond yang kamu butuhkan untuk mendapatkan skin LEGEND.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pt-2 row justify-content-center">
                    <div class="col-lg-5 col-sm-12">
                        <form>
                            <h5 class="text-white">Geser Sesuai Point Magic Wheel Anda</h4>
                            <div class="form-group">
                                <label for="inputwr" class="text-white pt-3">
                                    Point Magic Wheel Anda : <span id="f" style="font-weight:bold;color:#30cdf8"></span><span id='pointmwValue'></span>
                                </label>
                                <input type="range" class="form-control-range" min="0" max="199" value="100" id="pointmw" onchange="if (!window.__cfRLUnblockHandlers) return false; show_value2(this.value)" data-cf-modified-b1bbf0c8980ad5c57da61c3f-="">
                                <p class="pt-3 text-white">
                                    Membutuhkan Maksimal : <span id="slider_value2" style="color:white;font-weight:bold;"><i class="fas fa-gem" style="color: #00c8c8"></i>
                                </p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection