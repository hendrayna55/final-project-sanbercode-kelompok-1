<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.head')
    <title>Check Invoice</title>
</head>
<body style="background-color:#343a40">
    @include('layouts.navbar')

    <div class="container-fluid px-2 py-5">
        <div class="row justify-content-center">
            <div class="card-kanan col-lg-5">
                <div class="card m-auto bg-dark rounded">
                    <h5 class="card-header text-white">Check Invoice</h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <label class="text-white">Nomor Invoice : </label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-receipt"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="KS5467xxxxxx" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                                <div class="pt-1 float-right">
                                    <div class="btn btn-warning rounded">Check</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('layouts.footer')
    @include('layouts.js')
</body>
</html>