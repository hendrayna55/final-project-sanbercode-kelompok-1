@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="pt-4 col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-sm-12">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white" style="background-color: #282c30;">
                                Edit Profile
                            </h5>
                            <div class="card-body">
                                <form action="{{url('/profile/' . $user->id)}}" method="post">
                                    @csrf
                                    @method('put')

                                    <div class="d-none form-group row">
                                        <label for="role_id" class="col-sm-12 col-lg-3 col-form-label text-white">role_id</label>
                                        <div class="col-sm-12 col-lg-9">
                                            <input type="text" class="rounded form-control @error('role_id') is-invalid @enderror" id="role_id" name="role_id" 
                                                placeholder="Masukkan role_id Anda" value="{{old('role_id')?old('role_id'):$user->role_id}}">

                                            @error('role_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Masukkan role_id!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="nama" class="col-sm-12 col-lg-3 col-form-label text-white">Nama</label>
                                        <div class="col-sm-12 col-lg-9">
                                            <input type="text" class="rounded form-control @error('nama') is-invalid @enderror" id="nama" name="nama" 
                                                placeholder="Masukkan Nama Anda" value="{{old('nama')?old('nama'):$user->profile->nama}}">

                                            @error('nama')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Masukkan nama!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="username" class="col-sm-12 col-lg-3 col-form-label text-white">Username</label>
                                        <div class="col-sm-12 col-lg-9">
                                            <input type="text" class="rounded form-control @error('username') is-invalid @enderror" id="username" name="username" 
                                                placeholder="Masukkan Username Akun Anda" value="{{old('username')?old('username'):$user->username}}">

                                            @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Masukkan username!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email" class="col-sm-12 col-lg-3 col-form-label text-white">E - Mail</label>
                                        <div class="col-sm-12 col-lg-9">
                                            <input type="text" class="rounded form-control @error('email') is-invalid @enderror" id="email" name="email" 
                                                placeholder="Masukkan E-Mail Akun Anda" value="{{old('email')?old('email'):$user->email}}">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Masukkan email!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="nomor_wa" class="col-sm-12 col-lg-3 col-form-label text-white">Nomor WA</label>
                                        <div class="col-sm-12 col-lg-9">
                                            <input type="number" class="rounded form-control @error('nomor_wa') is-invalid @enderror" id="nomor_wa" name="nomor_wa" 
                                                placeholder="Masukkan Nomor WhatsApp Anda" value="{{old('nomor_wa')?old('nomor_wa'):$user->profile->nomor_wa}}">

                                            @error('nomor_wa')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Masukkan Nomor WhatsApp Akun!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <button type="submit" class="mt-3 btn rounded btn-success float-right">Submit</button>
                                </form>
                                <a href="{{url('/profile/' . $user->id)}}" class="mt-3 btn rounded btn-info">Kembali</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection