@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="pt-4 col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-lg-6 col-sm-12">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white" style="background-color: #282c30;">
                                Informasi Akun
                            </h5>
                            <div class="card-body">
                                <form action="{{url('/akun/' . $akun->id)}}" method="post">
                                    @csrf
                                    @method('put')

                                    <div class="d-none form-group row">
                                        <label for="user_id" class="col-sm-12 col-lg-3 col-form-label text-white">user_id</label>
                                        <div class="col-sm-12 col-lg-9">
                                            <input type="text" class="rounded form-control @error('user_id') is-invalid @enderror" id="user_id" name="user_id" placeholder="Masukkan user_id Akun Anda" value="{{$akun->user->id}}">

                                            @error('user_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Masukkan Nickname!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="nickname" class="col-sm-12 col-lg-3 col-form-label text-white">Nickname</label>
                                        <div class="col-sm-12 col-lg-9">
                                            <input type="text" class="rounded form-control @error('nickname') is-invalid @enderror" id="nickname" name="nickname" 
                                                placeholder="Masukkan Nickname Akun Anda" value="{{old('nickname')?old('nickname'):$akun->nickname}}">

                                            @error('nickname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Masukkan Nickname!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="id_akun" class="col-sm-12 col-lg-3 col-form-label text-white">ID Akun</label>
                                        <div class="col-sm-12 col-lg-9">
                                            <input type="number" class="rounded form-control @error('id_akun') is-invalid @enderror" id="id_akun" name="id_akun" 
                                                placeholder="Masukkan ID Akun Akun Anda" value="{{old('id_akun')?old('id_akun'):$akun->id_akun}}">

                                            @error('id_akun')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Masukkan ID Akun!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="server" class="col-sm-12 col-lg-3 col-form-label text-white">Server Akun</label>
                                        <div class="col-sm-12 col-lg-9">
                                            <input type="number" class="rounded form-control @error('server_akun') is-invalid @enderror" id="server_akun" name="server_akun" 
                                                placeholder="Masukkan Server Akun Anda" value="{{old('server_akun')?old('server_akun'):$akun->server_akun}}">

                                            @error('server_akun')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>Masukkan Server Akun!</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <button type="submit" class="mt-2 btn rounded btn-success float-right">Submit</button>
                                  </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection