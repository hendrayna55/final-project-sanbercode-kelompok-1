@extends('layouts.master')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="pt-4 col-lg-12">
                <div class="row justify-content-center">
                    <div class="col-lg-8 col-sm-12">
                        <div class="card m-auto bg-dark rounded">
                            <h5 class="card-header text-white" style="background-color: #282c30;">
                                Profile Akun
                            </h5>
                            <div class="card-body">
                                <div class="row justify-content-center">
                                    <div class="col-lg-8 col-sm-12">
                                        <table class="table table-dark">
                                            <tbody>
                                                <tr>
                                                    <th>Nama</th>
                                                    <td>{{Auth::user()->profile->nama}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Username</th>
                                                    <td>{{Auth::user()->username}}</td>
                                                </tr>
                                                <tr>
                                                    <th>E-mail</th>
                                                    <td>{{Auth::user()->email}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Role Akun</th>
                                                    @if (Auth::user()->role->id == 1)
                                                        <td>Super Admin</td>
                                                    @elseif(Auth::user()->role->id == 2)
                                                        <td>Reseller</td>
                                                    @else
                                                        <td>Pengguna</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <th>No WhatsApp</th>
                                                    @if (Auth::user()->profile->nomor_wa == null)
                                                        <td class="text-danger">Belum Update</td>
                                                    @else
                                                        <td>{{Auth::user()->profile->nomor_wa}}</td>
                                                    @endif
                                                </tr>
                                                <tr>
                                                    <th>Status</th>
                                                    @if (Auth::user()->verified_status == 1)
                                                        <td class="text-success">Terverifikasi</td>
                                                    @else
                                                        <td>Belum Terverifikasi</td>
                                                    @endif
                                                </tr>
                                            </tbody>
                                        </table>
                                        <a href="{{url('/profile/' . $user->id . '/edit')}}" class="btn btn-sm btn-success float-right rounded">Edit Profile</a>
                                    </div>
                                    <div class="col-12 my-3">
                                        <div class="text-white d-flex mx-auto align-content-center">
                                            <h5>Akun Mobile Legends</h5>
                                        </div>
                                        <table class="table table-bordered table-dark">
                                            <thead style="background-color: #212529;">
                                                <tr class="text-center align-middle">
                                                    <th>Nickname</th>
                                                    <th>ID-Server Akun</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if ($user->akuns()->exists())
                                                    @foreach ($akuns as $akun)
                                                    <tr class="text-center align-middle">
                                                        <td>{{$akun->nickname}}</td>
                                                        <td>{{$akun->id_akun}} - ({{$akun->server_akun}})</td>
                                                        <td>
                                                            <a href="{{url('/akun/' . $akun->id . '/edit')}}" class="btn btn-info btn-sm rounded mr-1">Edit</a>
                                                            <button class="btn btn-danger btn-sm rounded mx-1" data-toggle="modal" data-target="#modal-hapus{{$akun->id}}">Hapus</button>
                                                        </td>
                                                    </tr>

                                                    <!-- Modal Hapus Akun -->
                                                    <div class="modal fade" id="modal-hapus{{$akun->id}}">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header bg-danger">
                                                                    <h4 class="modal-title text-white">Hapus Akun</h4>
                                                                    <!-- <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button> -->
                                                                </div>
                                                                <div class="modal-body">
                                                                    <p class="">
                                                                        Nickname : {{$akun->nickname}}<br>
                                                                        ID-Server : {{$akun->id_akun}} ({{$akun->server_akun}})
                                                                        <br><br>Apakah anda yakin untuk menghapus data akun ini?
                                                                    </p>
                                                                </div>
                                                                <div class="modal-footer justify-content-between">
                                                                    <button type="button" class="btn btn-sm btn-primary rounded" data-dismiss="modal">Tutup</button>
                                                                    <form action="{{url('/akun/' . $akun->id)}}" method="post" class="">
                                                                        @csrf
                                                                        @method('DELETE')
                                                                        <button class="btn btn-danger btn-sm mr-2 rounded" data-toggle="modal" data-target="#modal-default">Hapus</button>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    <!-- /.modal -->
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan=3 class="align-middle text-center">Belum Ada Data</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                        <a href="{{url('/akun/' . $user->id . '/create')}}" class="btn btn-sm btn-success float-right rounded">Tambah Akun</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection