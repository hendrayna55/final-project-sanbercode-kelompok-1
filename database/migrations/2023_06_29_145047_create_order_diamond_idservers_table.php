<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_diamond_idservers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_id');
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('nickname');
            $table->string('id_akun');
            $table->string('server_akun');
            $table->string('bukti_pembayaran');
            $table->string('nomor_wa');
            $table->string('status');
            $table->unsignedBigInteger('diamond_idserver_id');
            $table->foreign('diamond_idserver_id')->references('id')->on('diamond_idservers')->onDelete('cascade');
            $table->unsignedBigInteger('payment_id');
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_diamond_idservers');
    }
};
