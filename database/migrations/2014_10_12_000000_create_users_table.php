<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('username');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->unsignedBigInteger('role_id')->default('3');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->boolean('verified_status')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });

        $passwordAdmin = bcrypt('superadmin');

        DB::table('users')->insert(
            array([
                'username' => 'superadmin',
                'email' => 'admin@gmail.com',
                'password' => $passwordAdmin,
                'role_id' => 1,
                'verified_status' => 1,
            ])
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
