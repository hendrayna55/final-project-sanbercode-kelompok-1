<?php

namespace App\Http\Middleware;

use Closure;

class CheckUserVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if($user)
            if($user->verified_status == true)
                return $next($request);
        
        
        return abort(401);
    }
}
