<?php

namespace App\Http\Controllers\login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
Use Alert;

class AuthController extends Controller
{

    public function index()
    {
        return view('auths.login');
    }

    public function login(Request $request)
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            $auth = Auth::user();

            if ($auth->level == 'admin') {
                alert()->success('Sukses','Berhasil Login');
                return redirect('admin/dashboard');
            } elseif ($auth->level == '2') {
                return redirect('mahasiswa/Beranda');
            }
        return redirect('/login');
        }
        else
            toast('Username dan Password Salah!!!','warning');
            return redirect('/login');
    }
    public function logout()
    {
        Auth::logout();
        alert()->success('Sukses','Berhasil Logout');
        return redirect('/login');
    }

}
