<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DiamondIdserver;
use App\Models\OrderDiamondIdserver;
use App\Models\Payment;
use App\Models\Akun;
use Auth;
use File;

class TopUpDiamondIDServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produks = DiamondIdserver::all();
        $payments = Payment::all();
        $akuns = Akun::where('user_id', Auth::user()->id)->get();
        return view('produk.topupid', compact('produks', 'payments', 'akuns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'diamond_idserver_id' => 'required',
            'nickname' => 'required',
            'id_akun' => 'required',
            'server_akun' => 'required',
            'payment_id' => 'required',
            'nomor_wa' => 'required',
            'bukti_pembayaran' => 'required',
        ]);

        $file = $request->file('bukti_pembayaran');
        $imgName = 'KS-TU-IdS-' . date('dmYHis') . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('/assets/bukti-pembayaran'), $imgName);

        OrderDiamondIdserver::create([
            'users_id' => Auth::user()->id,
            'nickname' => $request->nickname,
            'id_akun' => $request->id_akun,
            'server_akun' => $request->server_akun,
            'bukti_pembayaran' => $imgName,
            'nomor_wa' => $request->nomor_wa,
            'diamond_idserver_id' => $request->diamond_idserver_id,
            'payment_id' => $request->payment_id,
            'status' => $request->status,
        ]);

        alert()->success('Sukses','Berhasil Order Diamond');
        return redirect('/');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required',
            'diamond_idserver_id' => 'required',
            'nickname' => 'required',
            'id_akun' => 'required',
            'server' => 'required',
            'payment_id' => 'required'
        ]);

        if($request->hasFile('bukti_pembayaran')){
            $file = $request->file('bukti_pembayaran');
            $imgName = 'KS-TU-IdS-' . date('dmYHis') . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('assets/bukti-pembayaran'), $imgName);

            $order = DiamondIdserver::find($id);
            File::delete(public_path('/assets/bukti-pembayaran/' . $order->bukti_pembayaran));
        }

        DiamondIdserver::find($id)->update([
            'user_id' => Auth::user()->id,
            'diamond_idserver_id' => $request->diamond_idserver_id,
            'nickname' => $request->nickname,
            'id_akun' => $request->id_akun,
            'server' => $request->server,
            'payment_id' => $request->payment_id,
            'bukti_pembayaran' => $imgName ?? \DB::raw('bukti_pembayaran')
        ]);

        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Produk
    public function storeProduk(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
        ]);
    
        DiamondIdserver::create([
            'nama' => $request->nama,
            'harga' => $request->harga,
        ]);
    
        return redirect('/');
    }

    public function updateProduk(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
        ]);

        DiamondIdserver::find($id)->update([
            'nama' => $request->nama,
            'harga' => $request->harga,
        ]);

        return redirect('/');
    }

    //Payment
    public function storePayment(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'no_rekening' => 'required',
            'logo' => 'required'
        ]);

        $file = $request->file('logo');
        $imgName = 'Logo-' . $request->nama . '-' . date('dmYHis') . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('assets/img/logo'), $imgName);

        Payment::create([
            'nama' => $request->nama,
            'no_rekening' => $request->no_rekening,
            'logo' => $imgName,
        ]);

        return redirect('/');
    }

    public function updatePayment(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'no_rekening' => 'required',
            'logo' => 'required'
        ]);

        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $imgName = 'logo-' . $request->nama . '-' . date('dmYHis') . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('assets/img/logo'), $imgName);
            
            $data = Payment::find($id);
            File::delete(public_path('assets/img/logo/' . $imgName));
        }

        Payment::find($id)->update([
            'nama' => $request->nama,
            'no_rekening' => $request->no_rekening,
            'logo' => $imgName,
        ]);

        return redirect('/');
    }
}
