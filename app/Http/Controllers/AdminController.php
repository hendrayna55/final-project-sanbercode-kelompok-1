<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Profile;
use App\Models\Akun;
use App\Models\Payment;
use App\Models\DiamondIdserver;
use App\Models\OrderDiamondIdserver;
use Auth;
use Alert;
use File;

class AdminController extends Controller
{
    public function dashboard()
    {
        $akuns = Akun::all()->count();
        $users = User::all()->count() - 1;
        $payments = Payment::all()->count();
        $produks = DiamondIdserver::all()->count();
        $orders = OrderDiamondIdserver::all()->count();
        return view('admin.pages.dashboard', compact('akuns', 'users', 'payments', 'produks', 'orders'));
    }

    // Menampilkan Halaman akun
    public function akun()
    {
        $akuns = Akun::all();
        return view('admin.pages.akun', compact('akuns'));
    }

    public function createAkun()
    {
        $users = User::all();
        return view('admin.crud.akun.tambah_akun', compact('users'));
    }

    public function storeAkun(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'nickname' => 'required',
            'id_akun' => 'required',
            'server_akun' => 'required',
        ]);

        Akun::create([
            'user_id' => $request->user_id,
            'nickname' => $request->nickname,
            'id_akun' => $request->id_akun,
            'server_akun' => $request->server_akun,
        ]);

        alert()->success('Sukses','Berhasil Tambah Akun ' . $request->nickname);
        return redirect('/data-akun');
    }

    public function editAkun($id)
    {
        $akun = Akun::find($id);
        $users = User::all();
        return view('admin.crud.akun.edit_akun', compact('akun', 'users'));
    }

    public function updateAkun(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required',
            'nickname' => 'required',
            'id_akun' => 'required',
            'server_akun' => 'required',
        ]);

        Akun::find($id)->update([
            'user_id' => $request->user_id,
            'nickname' => $request->nickname,
            'id_akun' => $request->id_akun,
            'server_akun' => $request->server_akun,
        ]);

        alert()->success('Sukses','Berhasil Edit Akun ' . $request->nickname);
        return redirect('/data-akun');
    }

    public function destroyAkun($id)
    {
        Akun::find($id)->delete();
        
        alert()->success('Sukses','Berhasil Hapus Akun');
        return redirect('/data-akun');
    }

    // Menampilkan Halaman User
    public function user()
    {
        // $users = User::all();
        $users = User::where('role_id', 3)->get();
        return view('admin.pages.user', compact('users'));
    }

    public function createUser()
    {
        return view('admin.crud.user.tambah');
    }

    public function storeUser(Request $request)
    {
        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        Profile::create([
            'user_id' => $user->id,
            'nama' => "Belum Update",
            'nomor_wa' => null,
        ]);

        alert()->success('Sukses','Berhasil Tambah User ' . $request->username);
        return redirect('/user');
    }

    public function editUser($id)
    {
        $user = User::find($id);
        return view('admin.crud.user.edit', compact('user'));
    }

    public function updateUser(Request $request, $id)
    {
        // dd($request);
        User::find($id)->update([
            'role_id' => $request->role_id,
            'verified_status' => $request->verified_status,
            'username' => $request->username,
            'email' => $request->email,
            'password' => $request->password ?? \DB::raw('password')
        ]);

        alert()->success('Sukses','Berhasil Update User ' . $request->username);
        return redirect('/user');
    }

    public function destroyUser($id)
    {
        User::find($id)->delete();

        alert()->success('Sukses','Berhasil Hapus User');
        return redirect('/user');
    }

    //menampilkan halaman nominal
    public function nominal()
    {
        $produks = DiamondIdserver::all();
        return view('admin.pages.nominal', compact('produks'));
    }

    public function createProduk()
    {
        return view('admin.crud.nominal.tambahNominal');
    }

    public function storeProduk(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
        ]);

        DiamondIdserver::create([
            'nama' => $request->nama,
            'harga' => $request->harga,
        ]);

        alert()->success('Sukses','Berhasil Tambah Produk ' . $request->nama . ' Diamond');
        return redirect('/nominal');
    }

    public function editProduk($id)
    {
        $produk = DiamondIdserver::find($id);
        return view('admin.crud.nominal.editNominal', compact('produk'));
    }

    public function updateProduk(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
        ]);

        DiamondIdserver::find($id)->update([
            'nama' => $request->nama,
            'harga' => $request->harga,
        ]);

        alert()->success('Sukses','Berhasil Edit Produk ' . $request->nama . ' Diamond');
        return redirect('/nominal');
    }

    public function destroyProduk($id)
    {
        $produk = DiamondIdserver::find($id);
        DiamondIdserver::where('id', $id)->delete();

        alert()->success('Sukses','Berhasil Hapus Data Produk');
        return redirect('/nominal');
    }

    //menampilkan halaman pembayaran
    public function indexMetodePembayaran()
    {
        $methods = Payment::all();
        return view('admin.pages.metodePembayaran', compact('methods'));
    }

    public function createMetodePembayaran()
    {
        return view('admin.crud.metode-pembayaran.tambahMetodePembayaran');
    }

    public function storeMetodePembayaran(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'no_rekening' => 'required',
            'logo' => 'required|mimes:jpg,jpeg,png',
        ]);

        $file = $request->file('logo');
        $namaMetode = $request->nama;
        $imgName = date('dmYHis') . '-' . $namaMetode . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('/assets/logo-metode-pembayaran'), $imgName);

        Payment::create([
            'nama' => $request->nama,
            'no_rekening' => $request->no_rekening,
            'logo' => $imgName,
        ]);

        alert()->success('Sukses','Berhasil Tambah Metode Pembayaran ' . $namaMetode);
        return redirect('/metode-pembayaran');
    }

    public function editMetodePembayaran($id)
    {
        $method = Payment::find($id);
        return view('admin.crud.metode-pembayaran.editMetodePembayaran', compact('method'));
    }

    public function updateMetodePembayaran(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'no_rekening' => 'required',
            'logo' => 'mimes:jpg,jpeg,png',
        ]);

        $namaMetode = $request->nama;

        if($request->file('logo')){
            $file = $request->file('logo');
            $imgName = date('dmYHis') . '-' . $namaMetode . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('/assets/logo-metode-pembayaran'), $imgName);

            $data = Payment::find($id);
            File::delete(public_path('/assets/logo-metode-pembayaran/' . $data->logo));
        }

        Payment::find($id)->update([
            'nama' => $request->nama,
            'no_rekening' => $request->no_rekening,
            'logo' => $imgName ?? \DB::raw('logo'),
        ]);

        alert()->success('Sukses','Berhasil Edit Metode Pembayaran ' . $namaMetode);
        return redirect('/metode-pembayaran');
    }

    //menampilkan halaman order
    public function indexOrder()
    {
        $orders = OrderDiamondIdserver::all();
        return view('admin.pages.order', compact('orders'));
    }

    public function createOrder()
    {
        $users = User::all();
        $produks = DiamondIdserver::all();
        $payments = Payment::all();
        return view('admin.crud.order.createOrder', compact('users', 'produks', 'payments'));
    }

    public function storeOrder(Request $request)
    {
        $request->validate([
            'diamond_idserver_id' => 'required',
            'nickname' => 'required',
            'id_akun' => 'required',
            'server_akun' => 'required',
            'payment_id' => 'required',
            'nomor_wa' => 'required',
            'bukti_pembayaran' => 'required',
        ]);

        $file = $request->file('bukti_pembayaran');
        $imgName = 'KS-TU-IdS-' . date('dmYHis') . '.' . $file->getClientOriginalExtension();
        $file->move(public_path('/assets/bukti-pembayaran'), $imgName);

        OrderDiamondIdserver::create([
            'users_id' => $request->users_id,
            'nickname' => $request->nickname,
            'id_akun' => $request->id_akun,
            'server_akun' => $request->server_akun,
            'bukti_pembayaran' => $imgName,
            'nomor_wa' => $request->nomor_wa,
            'diamond_idserver_id' => $request->diamond_idserver_id,
            'payment_id' => $request->payment_id,
            'status' => $request->status,
        ]);

        alert()->success('Sukses','Berhasil Tambah Order');
        return redirect('/order');
    }

    public function editOrder($id)
    {
        $users = User::all();
        $produks = DiamondIdserver::all();
        $payments = Payment::all();
        $order = OrderDiamondIdserver::find($id);
        return view('admin.crud.order.editOrder', compact('order', 'users', 'produks', 'payments'));
    }

    public function updateOrder(Request $request, $id)
    {
        $request->validate([
            'diamond_idserver_id' => 'required',
            'nickname' => 'required',
            'id_akun' => 'required',
            'server_akun' => 'required',
            'status' => 'required',
            'payment_id' => 'required',
            'nomor_wa' => 'required'
        ]);

        OrderDiamondIdserver::find($id)->update([
            'users_id' => $request->users_id,
            'nickname' => $request->nickname,
            'id_akun' => $request->id_akun,
            'server_akun' => $request->server_akun,
            'bukti_pembayaran' => $imgName ?? \DB::raw('bukti_pembayaran'),
            'nomor_wa' => $request->nomor_wa,
            'diamond_idserver_id' => $request->diamond_idserver_id,
            'payment_id' => $request->payment_id,
            'status' => $request->status,
        ]);

        alert()->success('Sukses','Berhasil Edit Order');
        return redirect('/order');
    }

    public function destroyOrder($id)
    {
        $order = OrderDiamondIdserver::find($id);
        OrderDiamondIdserver::where('id', $id)->delete();

        alert()->success('Sukses','Berhasil Hapus Data Order');
        return redirect('/order');
    }
}
