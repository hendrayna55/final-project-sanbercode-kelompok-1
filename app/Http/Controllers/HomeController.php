<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OrderDiamondIdserver;
use App\Models\User;
use App\Models\DiamondIdserver;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        return view('index');
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cek()
    {
        return view('home');
    }

    public function dataOrder()
    {
        $orders = OrderDiamondIdserver::all();
        return view('dataOrder', compact('orders'));
    }

    public function contactUs()
    {
        return view('contactus');
    }
}
