<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;
use App\Models\Akun;
use Auth;
use Alert;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::find($id);
        $akuns = Akun::where('user_id', $id)->get();
        return view('profile.index', compact('user', 'akuns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createAkun($id)
    {
        $user = User::find($id);
        return view('profile.akun.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAkun(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'nickname' => 'required',
            'id_akun' => 'required',
            'server_akun' => 'required',
        ]);

        Akun::create([
            'user_id' => $request->user_id,
            'nickname' => $request->nickname,
            'id_akun' => $request->id_akun,
            'server_akun' => $request->server_akun,
        ]);

        alert()->success('Sukses','Berhasil Tambah Akun ' . $request->nickname);
        return redirect('/profile/' . $request->user_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editProfile($id)
    {
        $user = User::find($id);
        return view('profile.edit', compact('user'));
    }

    public function editAkun($id)
    {
        $akun = Akun::find($id);
        return view('profile.akun.edit', compact('akun'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function updateProfile(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'username' => 'required',
            'email' => 'required',
        ]);

        $user = User::find($id);
        
        User::find($id)->update([
            'username' => $request->username,
            'email' => $request->email,
            'role_id' => $user->role_id,
            'verified_status' => $user->verified_status,
        ]);

        Profile::where('user_id', $user->id)->update([
            'user_id' => $user->id,
            'nama' => $request->nama,
            'nomor_wa' => $request->nomor_wa,
        ]);

        alert()->success('Sukses','Berhasil Update Profile');
        return redirect('/profile/' . $user->id);
    }
    
    public function updateAkun(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required',
            'nickname' => 'required',
            'id_akun' => 'required',
            'server_akun' => 'required',
        ]);

        Akun::find($id)->update([
            'user_id' => $request->user_id,
            'nickname' => $request->nickname,
            'id_akun' => $request->id_akun,
            'server_akun' => $request->server_akun,
        ]);

        alert()->success('Sukses','Berhasil Update Akun ' . $request->nickname);
        return redirect('/profile/' . $request->user_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAkun($id)
    {
        $akun = Akun::find($id);
        Akun::where('id', $id)->delete();

        alert()->success('Sukses','Berhasil Hapus Akun');
        return redirect('/profile/' . Auth::user()->id);
    }
}
