<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDiamondIdserver extends Model
{
    use HasFactory;
    protected $table = 'order_diamond_idservers';
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function produk()
    {
        return $this->belongsTo(DiamondIdserver::class, 'diamond_idserver_id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Models\Payment');
    }
}
