<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiamondIdserver extends Model
{
    use HasFactory;
    protected $table = 'diamond_idservers';
    protected $guarded = ['id'];

    public function order()
    {
        return $this->hasMany(OrderDiamondIdserver::class, 'diamond_idserver_id');
    }
}
